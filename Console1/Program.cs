﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Console1
{
    class Program
    {
        static void Main(string[] args)
        {            
            /********************************************************************
             * Go to App.config and change the connection string to a proper one. 
             * Use a blank database, and in the package manager console run:
             * update-database -v
             * That should initialize your database and seed it. 
             *********************************************************************/
            Models.DataContext dc = new Models.DataContext();

            Console.WriteLine("Context Lazy {0}. Proxy Creation {1} ", dc.Configuration.LazyLoadingEnabled, dc.Configuration.ProxyCreationEnabled);

            Console.WriteLine("\n****************** Trying to use Lazy ************* "); 
            
            var grp = dc.Groups.FirstOrDefault();
            try
            {
                Console.WriteLine("GroupId {1}, AttrSet is null = {0}, name {2}", grp.AttrSet == null, grp.Id, grp.AttrSet.Name);
            }catch (Exception ex) {
            
                Console.WriteLine("Exception {0}\n", ex.Message); 
            }

            Console.WriteLine("\n****************** Using Eager ************* "); 
            var grp2 = dc.Groups.Include("AttrSet").FirstOrDefault();
            Console.WriteLine("GroupId {1}, AttrSet is null = {0}", grp2.AttrSet == null, grp2.Id); 

          
            Console.ReadKey(); 
        }
    }
}
