namespace Console1.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Console1.Models.DataContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Console1.Models.DataContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //

            context.AttrSets.AddOrUpdate(af => af.Id,
                new Models.AttrSet { Id = System.Guid.Parse("DEB83226-7A62-45A8-B18F-D21D360D6034"),
                                     Name = "Test1" }
                ); 

            context.Groups.AddOrUpdate ( g => g.Id , 
                new Models.Group { Id = System.Guid.Parse("186EBC8A-DEC7-4302-9F84-5A575577BAAC"), 
                                   Name = "Group1",
                                   AttrSetId = System.Guid.Parse("DEB83226-7A62-45A8-B18F-D21D360D6034")
                }
                ); 

        }
    }
}
