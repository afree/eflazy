namespace Console1.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Groups",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        AttrSetId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AttrSets", t => t.AttrSetId, cascadeDelete: true)
                .Index(t => t.AttrSetId);
            
            CreateTable(
                "dbo.AttrSets",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.Groups", new[] { "AttrSetId" });
            DropForeignKey("dbo.Groups", "AttrSetId", "dbo.AttrSets");
            DropTable("dbo.AttrSets");
            DropTable("dbo.Groups");
        }
    }
}
