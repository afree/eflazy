﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Console1.Models
{
    public class Group
    {
        public System.Guid Id { get; set; }
        public string Name { get; set; }

  
        public System.Guid AttrSetId { get; set; }    
        [ForeignKey("AttrSetId")]
        public virtual AttrSet AttrSet { get; set; }

      
        
    }
}
