﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Console1.Models
{
    public class AttrSet
    {
        public System.Guid Id { get; set; }
        public string Name { get; set; }

        
        public virtual ICollection<Group> InGroups { get; set; } 
    }
}
