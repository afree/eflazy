﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Console1.Models
{
    class DataContext : DbContext
    {
        public DataContext()
            : base("DefaultConnection")
        {            
        }

        public DbSet<Group> Groups { get;set;}
        public DbSet<AttrSet> AttrSets { get; set; }        

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Group>().HasRequired(x => x.AttrSet).WithMany(x => x.InGroups); 
        }
    }
}
